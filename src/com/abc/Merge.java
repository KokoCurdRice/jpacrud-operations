package com.abc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.Prod;

public class Merge {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		Prod p = new Prod(105, "New Printer", 12000, 10);
		
		em.merge(p);
		
		em.getTransaction().commit();
		
		em.close();
		factory.close();
	}
}
