package com.abc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.Prod;

public class Test {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		
		Prod p = new Prod(101, "computer", 35000, 10);
		Prod p1 = new Prod(102, "computer1", 30000, 10);
		Prod p2 = new Prod(103, "computer2", 40000, 10);
		em.persist(p);
		em.persist(p1);
		em.persist(p2);
		
		
		em.getTransaction().commit();
		em.close();
		factory.close();
		
	}
}
