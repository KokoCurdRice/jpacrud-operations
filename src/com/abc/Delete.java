package com.abc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.Prod;

public class Delete {
public static void main(String[] args) {
	EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
	EntityManager em = factory.createEntityManager();
	
	em.getTransaction().begin();
	
	Prod temp = em.find(Prod.class, 104);
	if(temp != null ) {
		em.remove(temp);
	} else {
		System.out.println("Could not delete product as it is not available");
	}
	
	
	em.getTransaction().commit();
	em.close();
	}
}
