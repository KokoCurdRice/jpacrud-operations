package com.abc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.Prod;

public class Update {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		Prod temp = em.find(Prod.class, 102);
		
		if(temp != null) {
		temp.setPname("dell Laptop");
		temp.setPrice(77000);
		}
		else {
			System.out.println("Could not update the product as it is not available");
		}
		em.getTransaction().commit();
		
		em.close();
		factory.close();
	}
}
