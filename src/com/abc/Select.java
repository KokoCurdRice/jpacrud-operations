package com.abc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.Prod;

public class Select {
	public static void main(String[] args) {
		new Select().selectMethod();
	}
	public void selectMethod() {
	EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
	EntityManager em = factory.createEntityManager();
	
	Prod temp = em.find(Prod.class, 102); //select * from product where id = 102
	//em.find();
	
	System.out.println(temp);
	
	em.close();
	factory.close();
	}
}
