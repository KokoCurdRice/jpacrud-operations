package com.accenture;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Prod {
	@Id
	private int pid;
	private String pname;
	private int price;
	private int quant;
	public Prod(int pid, String pname, int price, int quant) {
		super();
		this.pid = pid;
		this.pname = pname;
		this.price = price;
		this.quant = quant;
	}
	public Prod() {
		super();
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuant() {
		return quant;
	}
	public void setQuant(int quant) {
		this.quant = quant;
	}
	@Override
	public String toString() {
		return "Prod [pid=" + pid + ", pname=" + pname + ", price=" + price + ", quant=" + quant + "]";
	}
	
	
}
