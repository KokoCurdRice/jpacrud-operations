package com.accenture.test;
import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.accenture.Prod;

public class SelectTest {
	EntityManagerFactory fact = null;
	EntityManager em = null;
	@BeforeAll
	public void setup() {
		fact = Persistence.createEntityManagerFactory("unit1");	
		em = fact.createEntityManager();
	}
	@Test
	public void selectTest() {
		
		
		Prod p = em.find(Prod.class, 116);
		assertEquals(116, p.getPid());
		assertEquals("Monitor", p.getPname());
		assertEquals(35000,p.getPrice());
	}
		@AfterAll
		public void close() {
			em.close();
			fact.close();
	}
}
